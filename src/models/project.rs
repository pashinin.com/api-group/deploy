use serde::{Serialize,Deserialize};
use tracing::error;
use uuid::Uuid;
use crate::models::env::ProjectEnvVar;

/// Project at Gitlab
#[derive(Debug,Serialize,Deserialize,sqlx::FromRow,Clone)]
pub struct Project {
    pub id: Uuid,

    /// Example: "https://gitlab.com/pashinin.com/ui/admin-rs"
    pub url: String,

    pub private: bool,
}

impl Default for Project {
    fn default() -> Self {
        Self {
            id: Uuid::default(),
            url: "".to_string(),
            private: true,
        }
    }
}

impl Project {
    /// Fetch Project by URL or create a new one
    pub async fn get_or_create(url: &str, pool: &sqlx::Pool<sqlx::Postgres>) -> Result<Self, sqlx::Error> {
        match sqlx::query_as(
            r"SELECT id, url, private FROM admin_projects WHERE url=$1")
            .bind(url)
            .fetch_one(pool).await {
                Ok(project) => Ok(project),
                Err(e) => {
                    let p = Project {
                        id: Uuid::new_v4(),
                        url: url.to_string(),
                        ..Project::default()
                    };

                    match e {
                        // If project does not exist - insert a new one
                        sqlx::Error::RowNotFound => {
                            match p.insert(&pool).await {
                                Ok(project) => Ok(project),
                                Err(err) => {
                                    error!("{}", err);
                                    Err(err)
                                }
                            }
                        }
                        _ => {
                            let msg = format!("Unknown error: {}", e);
                            error!("{}", msg);
                            Err(e)
                        },
                    }
                },
            }
    }

    pub async fn insert(&self, pool: &sqlx::Pool<sqlx::Postgres>) -> Result<Project, sqlx::Error> {
        sqlx::query_as(&format!(
            "INSERT INTO admin_projects (id, url, private) VALUES ($1, $2, $3)
RETURNING id, url, private"))
            .bind(self.id)
            .bind(&self.url)
            .bind(self.private)
            .fetch_one(pool).await
            .map(|user| user)
    }

    pub async fn get_env_vars(
        &self,
        pool: &sqlx::Pool<sqlx::Postgres>
    ) -> Result<Vec<ProjectEnvVar>, sqlx::Error> {
        sqlx::query_as("SELECT project_id, deployment_id, key, value FROM admin_projects_env_vars WHERE project_id=$1")
            .bind(self.id)
            .fetch_all(pool).await
            .map(|vars| vars)
    }
}



/// Domain
#[derive(Debug,Serialize,sqlx::FromRow,Clone)]
pub struct Domain {
    pub id: Uuid,

    /// Example: "pashinin.com"
    pub name: String,
    // pub private: bool,
}

/// DNS record
/// There can be multiple DNS records for each domain
pub struct DNSRecord {
    // pub domain_id: Uuid,

    /// String like "A", "CNAME", "TXT" and others
    pub record_type: String,

    /// Hostname in DNS record
    ///
    ///
    pub name: String,

    pub value: String,
    pub ttl: u32,
}
