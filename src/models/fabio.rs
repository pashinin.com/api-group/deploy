use std::fmt;
use serde::{Serialize,Deserialize};

/// urlprefix-api.pashinin.com:443/deploy allow=ip:10.254.239.0/24 strip=/{{hostname}}
/// urlprefix-test.pashinin.com:443/ allow=ip:10.254.239.0/24
#[derive(Debug,Default,Serialize,Deserialize,Clone)]
pub struct FabioTag {
    /// api.pashinin.com:443/deploy
    pub endpoint: String,

    /// allow=ip:10.254.239.0/24
    pub lan_only: bool,

    pub strip: Option<String>,
}

impl FabioTag {
    pub fn from_params(group: &str, tag: &str, hostname: &str) -> Self {
        Self {
            endpoint: match group {
                "api" => match tag.as_ref() {
                    "latest" => format!("api-latest.pashinin.com:443/{}", hostname),
                    _ => format!("api.pashinin.com:443/{}", hostname),
                }
                "ui" => match tag.as_ref() {
                    "latest" => format!("{}-latest.pashinin.com:443/", hostname),
                    _ => format!("{}.pashinin.com:443/", hostname),
                },
                _ => "".to_string(),
            },
            lan_only: tag == "latest",
            strip: match group {
                "api" => Some(format!("/{}", hostname)),
                _ => None,
            },
        }
    }
}


impl fmt::Display for FabioTag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "urlprefix-{} allow=ip:10.254.239.0/24 {}",
               self.endpoint,
               self.strip.as_ref().map(|x| format!("strip={}", x)).unwrap_or("".to_string())
        )
    }
}
