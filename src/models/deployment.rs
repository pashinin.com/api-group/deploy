use serde::{Serialize,Deserialize};
use uuid::Uuid;

/// Existing deployment
///
/// Each project can have multiple deployments:
///
///  1. Different tags at different endpoints
///  2. The same tag has several endpoints
///
///
/// API project:
///
/// v1.0.0 at api.pashinin.com/project/v1.0.0/
///
/// v2.0.0 at api.pashinin.com/project/v2.0.0/
///        at api.pashinin.com/project/
///
/// A new tag "v3.0.0" will just create an additional endpoint: /v3.0.0
///
///
/// UI project:
///
/// v1.0.0 at test.pashinin.com
/// v2.0.0 at another-project.pashinin.com
///
/// A new tag "v3.0.0" changes all endpoints:
///
/// v3.0.0 at test.pashinin.com
/// v3.0.0 at another-project.pashinin.com
///
/// Of course with canaries and auto_promote = true.
#[derive(Debug,Serialize,Deserialize,sqlx::FromRow,Clone)]
pub struct Deployment {
    // pub project: Option<Project>,
    pub project_id: Uuid,

    /// api.pashinin.com/captcha/v0.1.14/
    pub endpoint: String,

    /// v0.1.14
    pub tag: String,

    pub disabled: bool,
}

static FIELDS: &'static str = "project_id, endpoint, tag, disabled";

impl Deployment {
    pub async fn insert(&self, pool: &sqlx::Pool<sqlx::Postgres>) -> Result<Deployment, sqlx::Error> {
        sqlx::query_as(&format!(
            "INSERT INTO admin_projects_deployments (project_id, endpoint, tag, disabled)
VALUES ($1, $2, $3, $4) RETURNING {}", FIELDS))
            .bind(self.project_id)
            .bind(&self.endpoint)
            .bind(&self.tag)
            .bind(&self.disabled)
            .fetch_one(pool).await
            .map(|object| object)
    }
}
