//! Env

use serde::Serialize;
use uuid::Uuid;

/// Env variable for a project
#[derive(Debug,Serialize,sqlx::FromRow,Clone)]
pub struct ProjectEnvVar {
    // pub id: Uuid,
    pub project_id: Uuid,
    pub deployment_id: Option<Uuid>,
    pub key: String,
    pub value: String,
}
