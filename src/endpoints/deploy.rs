use actix_web::{web, HttpRequest, HttpResponse, Error, post};
use chrono::{Utc};
use nomad::resources::{NetworkResource, Port};
use nomad;
use serde::Deserialize;
use tracing::{error, info, warn};
use url::Url;

use api_deploy_rs;
use api_deploy_rs::models::{Project, Deployment, FabioTag};

use crate::auth;
use crate::config::Config;
use crate::endpoints::get_default_endpoint;
use nomad::vault::Vault;

#[derive(Deserialize)]
pub struct DeploymentRequest {
    project_url: String,
    tag: Option<String>,
}

#[post("/")]
pub async fn deploy(
    request: HttpRequest,
    current_deployment: web::Json<DeploymentRequest>,
    config: web::Data<Config>,
    pool: web::Data<sqlx::Pool<sqlx::Postgres>>,
) -> Result<HttpResponse, Error> {
    // Auth
    if let Err(msg) = auth::allowed_to_deploy(request, &config) {
        return Ok(HttpResponse::BadRequest().body(msg));
    }

    // Get project from database or insert a new one
    let project = match Project::get_or_create(
        &current_deployment.project_url,
        &pool,
    ).await {
        Ok(project) => project,
        Err(e) => {
            error!("{}", e);
            return Ok(HttpResponse::BadRequest().body(
                format!("Error fetching/creating project: {}", e)
            ));
        }
    };

    // Parse URL of deployed project
    let parsed_url = match Url::parse(&current_deployment.project_url) {
        Ok(parsed) => parsed,
        Err(err) => return Ok(HttpResponse::BadRequest().body(
            format!("Can't parse url: {}", err)
        ))
    };
    let parts: Vec<&str> = parsed_url.path()[1..].split("/").collect();
    let project_name = match parts.last() {
        Some(name) => name,
        None => {
          return Ok(HttpResponse::BadRequest().body(
              format!("Can't get project name from url")
          ))
        }
    };

    // Strip "-rs" part from project to make a hostname
    //
    // "api-auth-rs" => "api-auth"
    let hostname: String = match project_name.ends_with("-rs") {
        true => project_name.chars().skip(0).take(project_name.len()-3).collect(),
        false => project_name.to_string(),
    };

    let group_name = match parts.get(parts.len() - 2) {
        Some(name) => {
            match name {
                &"api-group" => "api",
                _ => name,  // "ui"
            }
        },
        None => {
          return Ok(HttpResponse::BadRequest().body(
              format!("Can't get project name from url")
          ))
        }
    };


    // Decide which Docker image is going to be deployed ("latest", "v1.0.0")
    let mut tag = current_deployment.tag.clone().unwrap_or_default();
    if tag == "" {
        tag = "latest".to_string();
    }



    // JobID
    //
    // To choose JobID I need to decide how deployments will be
    // organized.
    //
    // It can be:
    //   1. One job, multiple groups for each tag (version)
    //   2. Multiple jobs for each tag (version)
    //
    // I do not want many jobs for each deployment.  But what to do if 1
    // group fails in multi-group job?  "latest" group must be without
    // health checks since it can always fail. When deploying a new
    // stable group and it fails need to auto_revert to previous version
    // of a job.
    //
    // let job_id = format!("{}-{}-{}", group_name, project_name, tag);
    let job_id = format!("{}-{}", group_name, project_name);

    let mut job = nomad::jobs::Job {
        id: job_id,
        region: None,
        datacenters: vec!["moscow".to_string()],
        constraint: vec![nomad::constraint::Constraint {
            attribute: Some("${meta.instance_type}".to_string()),
            value: Some("worker".to_string()),
            operator: Some("=".to_string()),
        }],
        task_groups: vec![],
        ..nomad::jobs::Job::default()
    };

    let mut deployments: Vec<Deployment> = match sqlx::query_as(
        r"SELECT project_id, endpoint, tag, disabled FROM admin_projects_deployments WHERE project_id=$1")
        .bind(project.id)
        .fetch_all(&**pool).await {
            Ok(data) => data,
            Err(err) => {
                error!("deployments: {}", err);
                return Ok(HttpResponse::BadRequest().body(format!("deployments: {}", err)));
            },
        };

    // dbg!(&deployments);

    // Add current deployment to all deployments
    let no_current_tag = deployments.iter().find(|&x| x.tag == tag).is_none();
    let have_stable_running = deployments.iter()
        .find(|&x| x.tag != "latest" && !x.disabled).is_some();
    if no_current_tag {
        info!("No tag {}. Adding to DB...", &tag);
        let d = Deployment {
            endpoint: get_default_endpoint(group_name, &tag, &hostname),
            project_id: project.id,
            tag: tag.to_string(),
            disabled: false,
        };
        match d.insert(&pool).await {
            Ok(d) => d,
            Err(err) => {
                error!("{}", err);
                return Ok(HttpResponse::BadRequest().body(
                    format!("Error inserting deployment: {}", err)
                ));
            }
        };
        deployments.push(d);
    } else {
        info!("Tag \"{}\" is already deployed", tag);
    }


    // Get all env vars for a project
    let all_vars = project.get_env_vars(&pool).await.unwrap();

    for deployment in deployments.iter() {
        let fabio_tag = FabioTag::from_params(&group_name, &deployment.tag, &hostname);
        let mut tags = vec![fabio_tag.to_string()];
        if !have_stable_running && &deployment.tag == "latest" {
            tags.push(FabioTag::from_params(&group_name, "", &hostname).to_string());
        }

        let mut templates: Vec<nomad::tasks::Template> = vec![];

        // Environment variables
        let mut v: Vec<String> = vec![];
        for var in all_vars.iter() {
            v.push(format!("{}=\"{}\"", var.key, var.value));
        }

        // Insert DATE to force update "latest" tasks.  Wihtout it Nomad
        // job has no changes and nothing happens.
        if &deployment.tag == "latest" {
            v.push(format!("DATE=\"{}\"", Utc::now().to_rfc3339()));
        }

        // Create file.env - a template with all env variables
        if v.len() > 0 {
            templates.push(nomad::tasks::Template {
                data: v.join("\n"),
                destination: Some("${NOMAD_SECRETS_DIR}/file.env".to_string()),
                env: Some(true),
            });
        }

        // Docker image
        let image = match group_name {
            "api" => format!("registry.gitlab.com/pashinin.com/api-group/{}:{}",
                             project_name,
                             deployment.tag.clone()),
            _ => format!("registry.gitlab.com{}:{}",
                         &parsed_url.path(),
                         deployment.tag.clone()),
        };

        // Add group
        job.task_groups.push(nomad::tasks::TaskGroup {
            name: deployment.tag.clone(),
            count: match deployment.tag.as_ref() {
                "latest" => None,
                _ => Some(2),
            },
            network: vec![
                NetworkResource {
                    mode: Some("bridge".to_string()),
                    port: vec![Port{ label: "http".to_string() }],
                }
            ],
            services: vec![nomad::services::Service {
                name: format!("{}-{}", group_name, project_name),
                tags: tags.clone(),
                check: vec![
                    nomad::services::ServiceCheck {
                        name: None,
                        _type: "http".to_string(),
                        path: Some("/healthcheck".to_string()),
                        interval: Some(20 * 1000_000_000),  // 20s
                        timeout: Some(2 * 1000_000_000),  // 2s
                    }
                ],
                port: Some("http".to_string()),
            }],
            tasks: vec![
                nomad::tasks::Task {
                    name: project_name.to_string(),
                    driver: "docker".to_string(),
                    config: nomad::tasks::TaskConfig {
                        image: image,
                        auth: match project.private {
                            true => {
                                if !config.can_deploy_private_projects() {
                                    warn!("Can't deploy private project: {}", project.url);
                                    warn!("  CI_DEPLOY_USER or CI_DEPLOY_PASSWORD not set.");
                                    return Ok(HttpResponse::BadRequest().body(
                                        "Can't deploy private project without CI_DEPLOY_USER or CI_DEPLOY_PASSWORD"
                                    ));
                                }
                                Some(nomad::tasks::TaskConfigAuth {
                                    server_address: "registry.gitlab.com".to_string(),
                                    username: config.ci_deploy_user.clone(),
                                    password: config.ci_deploy_password.clone(),
                                })
                            },
                            false => None,
                        }
                    },
                    templates: templates.clone(),
                    vault: Some(Vault {
                        policies: Some(vec!["site".into()]),
                        ..Vault::default()
                    }),
                }
            ],
        });
    }

    // Send job to Nomad
    match nomad::jobs::register(&job).await {
        Ok(response) => Ok(HttpResponse::Ok().json(response)),
        Err(err) => Ok(HttpResponse::BadRequest().body(format!("{}", err)))
    }
}
