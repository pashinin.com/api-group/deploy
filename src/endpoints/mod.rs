pub mod healthcheck;
// use healthcheck::*;


pub mod deploy;
// use deploy::deploy;


pub fn get_default_endpoint(group: &str, tag: &str, hostname: &str) -> String {
    match tag {
        "latest" => match group {
            "api" => format!("api-latest.pashinin.com/{}", hostname),
            "ui" => format!("{}-latest.pashinin.com", hostname),
            _ => panic!("wtf"),
        },
        _ => match group {
            "api" => format!("api.pashinin.com/{}", hostname),
            "ui" => format!("{}.pashinin.com", hostname),
            _ => panic!("wtf"),
        },
    }
}
