use actix_web::{web, Responder, get};
use serde::{Serialize};
use std::env;

#[derive(Serialize)]
struct Info<'a> {
    service: &'a str,
    tag: &'a str,
    description: &'a str,
    commit_short_sha: &'a str,
}

#[get("/healthcheck")]
pub(crate) async fn healthcheck() -> impl Responder {
    web::Json(Info{
        service: env!("CARGO_PKG_NAME"),
        tag: match option_env!("CI_COMMIT_TAG") {
            Some(tag) => tag,
            None => match option_env!("CI_COMMIT_REF_NAME") {
                Some(branch) => match branch {
                    "master" => "latest",
                    _ => branch
                },
                None => "unknown branch",
            },
        },
        description: env!("CARGO_PKG_DESCRIPTION"),
        commit_short_sha: match option_env!("CI_COMMIT_SHORT_SHA") {
            Some(sha) => sha,
            None => "",
        },
    })
}
