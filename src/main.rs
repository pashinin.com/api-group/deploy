use actix_web::{web, App, HttpServer, middleware};
use common;
use std::env;
use tracing::{warn, info};

mod auth;
mod config;
mod endpoints;
mod error;

#[actix_web::main]
async fn main() {
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    // Connect to database
    let pool: sqlx::Pool<sqlx::Postgres> = match common::sql::connect().await {
        Ok(pool) => pool,
        Err(_err) => return,  // error will be logged in connect() function
    };

    let config = config::Config::default();
    if !config.can_deploy_private_projects() {
        warn!("Can't deploy private projects:");
        warn!("  CI_DEPLOY_USER or CI_DEPLOY_PASSWORD not set.");
    }

    let port = env::var("NOMAD_PORT_http").unwrap_or(String::from("8080"));
    let addr = format!("0.0.0.0:{}", port);

    info!("{}, {}", env!("CARGO_PKG_NAME"), addr);
    HttpServer::new(move || {
        App::new()
            .app_data(config.clone())
            .wrap(common::cors())
            .wrap(middleware::Logger::default())
            .app_data(pool.clone())
            .service(endpoints::healthcheck::healthcheck)
            .service(endpoints::deploy::deploy).app_data(
                web::JsonConfig::default().error_handler(error::json_error_handler),
            )
    })
        .bind(addr).unwrap()
        .run()
        .await.unwrap()
}
