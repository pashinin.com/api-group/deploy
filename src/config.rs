use std::env;

use crate::auth;

#[derive(Debug,Clone)]
pub struct Config {
    pub auth_token: String,

    // Credentials used to download Docker images from private Gitlab projects
    pub ci_deploy_user: String,
    pub ci_deploy_password: String,
}

impl Config {
    pub fn can_deploy_private_projects(&self) -> bool {
        self.ci_deploy_user != "" && self.ci_deploy_password != ""
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            auth_token: auth::get_deploy_token(),
            ci_deploy_user: env::var("CI_DEPLOY_USER").unwrap_or(String::from("")),
            ci_deploy_password: env::var("CI_DEPLOY_PASSWORD").unwrap_or(String::from("")),
        }
    }
}
