use actix_web::{HttpRequest, web};
use std::env;

#[cfg(not(debug_assertions))]
use tracing::error;

use crate::config::Config;

#[cfg(not(debug_assertions))]
use constant_time_eq::constant_time_eq;

/// Gets DEPLOY_TOKEN env variable.
///
/// Without it any deployment is aborted. If
///
/// Ignored in DEBUG mode.
pub fn get_deploy_token() -> String {
    #[cfg(debug_assertions)]
    return env::var("DEPLOY_TOKEN").unwrap_or_default();
    #[cfg(not(debug_assertions))]
    return env::var("DEPLOY_TOKEN").unwrap_or(
        match common::vault::vault_get("DEPLOY_TOKEN") {
            Some(v) => v,
            None => panic!("DEPLOY_TOKEN not set. Can't get it from Vault"),
        }
    );
}

/// Checks if deployment request is good to go.
#[cfg(debug_assertions)]
pub fn allowed_to_deploy(_req: HttpRequest, _data: &web::Data<Config>) -> Result<bool, String> {
    Ok(true)
}
#[cfg(not(debug_assertions))]
pub fn allowed_to_deploy(req: HttpRequest, data: &web::Data<Config>) -> Result<bool, String> {
    match req.headers().get("DEPLOY_TOKEN") {
        Some(token) => if constant_time_eq(token.as_bytes(), data.auth_token.as_bytes()) {
            Ok(true)
        } else {
            error!("Incorrect DEPLOY_TOKEN");
            return Err(format!("Incorrect DEPLOY_TOKEN"));
        },
        None => {
            error!("DEPLOY_TOKEN must be set");
            return Err(format!("DEPLOY_TOKEN must be set"));
        },
    }
}
