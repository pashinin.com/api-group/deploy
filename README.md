# Deployment microservice

## Development

Start as a part of docker-compose `dev` project:

```
cd ~/src/dev; ./run api-deploy-rs
```

or

```
cargo install cargo-watch
DEPLOY_TOKEN=123 cargo watch -x 'run'
DEPLOY_TOKEN=123 CI_DEPLOY_USER=... CI_DEPLOY_PASSWORD=... cargo watch -x 'run'
```



## Usage

`POST /deploy` - deploy given project

Payload (JSON):

```
{
  "project_name": "https://gitlab.com/pashinin.com/api-group/deploy",
  "tag": "v1.0.0"
}
```

Self deploy (empty tag == "latest")

```bash
curl -L -X POST -H 'Content-Type: application/json' -H 'DEPLOY_TOKEN: 123' \
--data '{"project_url": "https://gitlab.com/pashinin.com/api-group/deploy"}' \
http://api.localhost/deploy/
```

Other endpoints:

`GET /healthcheck` - info and healthcheck


## Environment variables

`DEPLOY_TOKEN` - verification token. Only those who supply this token
are allowed to run deployments. Must be set. Can be set in Gitlab's
group settings for all projects.

`CI_DEPLOY_USER`, `CI_DEPLOY_PASSWORD` - set those for deployment
service to be able to access private Docker images (in private repos). I
store these credentials in Hashicorp Vault. Generate them in Gitlab's
project (or group) settings `Settings > Repository > Deploy tokens`.

`NOMAD_ENDPOINT` (`http://127.0.0.1:4646`) - address of Nomad server.
