# Build Stage
# docker build -t registry.gitlab.com/pashinin.com/api-group/deploy:master .
FROM pashinin/rust-runtime:2020-09-21
COPY target/release/api-deploy-rs ./
CMD ["./api-deploy-rs"]
